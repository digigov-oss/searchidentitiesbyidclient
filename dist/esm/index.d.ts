import { AuditRecord, AuditEngine } from "@digigov-oss/gsis-audit-record-db";
export type AuditInit = AuditRecord;
export type SearchIdentitiesByIdOutputRecord = {
    status: "0" | "1";
    errorCode?: string;
    errorMessage?: string;
    errorDescription?: string;
    recordsNumber: number;
    transactionId: 0 | number;
    identities: {
        identity: Identity[];
    };
};
export type ServiceOutputRecord = {
    searchIdentitiesByIdOutputRecord: SearchIdentitiesByIdOutputRecord;
    callSequenceId: number;
    callSequenceDate: string;
    errorRecord: ErrorRecord | null;
    auditRecord: AuditRecord;
};
export type Identity = {
    birthDate: string;
    birthPlace: string;
    cancelReason?: {
        id: string;
        description: string;
        cancelDate: string;
    };
    fatherName: string;
    fatherNameLatin: string;
    gender: "Α" | "Γ";
    idnumber: string;
    isActive: boolean | "true" | "True" | "false" | "False";
    issueDate: string;
    issueInstitution: {
        id: string;
        description: string;
    };
    lastUpdateDate: string;
    motherName: string;
    municipality?: string;
    name: string;
    nameLatin: string;
    populationRegistryNo?: string;
    previousId?: string | null;
    previousIdNumber: string;
    surname: string;
    surnameLatin: string;
};
export type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};
export type SearchIdentitiesByIdInputRecord = {
    adt: string;
    reasonDesc: string;
};
/**
 * @type Overrides
 * @description Overrides for the SOAP client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 */
export type overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
/**
 *
 * @param input searchIdentitiesByNameInputRecord;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns searchIdentitiesByNameOutputRecord | errorRecord
 */
export declare const getIdentitys: (input: SearchIdentitiesByIdInputRecord, user: string, pass: string, overrides?: overrides | undefined) => Promise<ServiceOutputRecord>;
export default getIdentitys;
