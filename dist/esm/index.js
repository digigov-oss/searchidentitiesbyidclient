import soapClient from "./soapClient.js";
import { generateAuditRecord, FileEngine, } from "@digigov-oss/gsis-audit-record-db";
import config from "./config.json";
/**
 *
 * @param input searchIdentitiesByNameInputRecord;
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 * @returns searchIdentitiesByNameOutputRecord | errorRecord
 */
export const getIdentitys = async (input, user, pass, overrides) => {
    const endpoint = overrides?.endpoint ?? "";
    const prod = overrides?.prod ?? false;
    const auditInit = overrides?.auditInit ?? {};
    const auditStoragePath = overrides?.auditStoragePath ?? "/tmp";
    const auditEngine = overrides?.auditEngine ?? new FileEngine(auditStoragePath);
    const wsdl = prod == true ? config.prod.wsdl : config.test.wsdl;
    const auditRecord = await generateAuditRecord(auditInit, auditEngine);
    if (!auditRecord)
        throw new Error("Audit record is not initialized");
    try {
        const s = new soapClient(wsdl, user, pass, auditRecord, endpoint);
        const Identitys = await s.getIdentitys(input);
        return { ...Identitys, ...auditRecord };
    }
    catch (error) {
        throw error;
    }
};
export default getIdentitys;
