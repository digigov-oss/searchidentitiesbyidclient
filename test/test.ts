import getIdentitys from '../src/index';
import config from './config.json'; 
import inspect from 'object-inspect';
const test = async () => {

   const input ={
    adt: "ΑΔ100000", 
    reasonDesc:"ΔΟΚΙΜΗ ΑΝΑΖΗΤΗΣΗΣ"
   }

    try {
        const Identitys = await getIdentitys(input, config.user, config.pass);
        return Identitys;
    } catch (error) {
        console.log(error);
    }
}

test().then((record) => { console.log(inspect(record,{depth:10,indent:"\t"})); });